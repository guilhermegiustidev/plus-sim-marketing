export const locale = {
    lang: 'pt',
    data: {
       'LABELS': {
            'PAGE' : 'Representantes',
            'CUSTOMERS': 'Representantes',
            'CUSTOMERS_HEADER_TEXT': 'Encontre um representante mais proximo',
            'CUSTOMERS_SEARCH': {
                'LIST': 'Representantes',
                'MAP': 'Busque Representante'
            },
            'LOCATION': {
                'HEADER': 'Informe a localização',
                'CONTRY': 'País',
                'STATE': 'Estado',
                'CITY': 'Cidade',
                'SEARCH': 'Buscar Representante',
                'MAP': 'Localização'
            },
            'CONTRY': {
                'ALL': 'Todos',
                'BRAZIL': 'Brasil',
                'USA': 'Estados Unidos'
            },
            'STATE': {
                'ALL': 'Todos',
            },
            'CITY': {
                'ALL': 'Todas',
            },
       }
    }
};
