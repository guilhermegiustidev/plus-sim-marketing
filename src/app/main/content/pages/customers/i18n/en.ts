﻿export const locale = {
    lang: 'en',
    data: {
       'LABELS': {
            'PAGE' : 'Sales Agents',
            'CUSTOMERS': 'Sales Agents',
            'CUSTOMERS_HEADER_TEXT': 'Find the nearest Sales Agent ',
            'CUSTOMERS_SEARCH': {
                'LIST': 'Sales Agents',
                'MAP': 'Find a Sales Agent'
            },
            'LOCATION': {
                'HEADER': 'Your Location',
                'CONTRY': 'Country',
                'STATE': 'State',
                'CITY': 'City',
                'SEARCH': 'Search',
                'MAP': 'Location'
            },
            'CONTRY': {
                'ALL': 'All',
                'BRAZIL': 'Brazil',
                'USA': 'United States'
            },
            'STATE': {
                'ALL': 'All',
            },
            'CITY': {
                'ALL': 'All',
            },
       }
    }
};
