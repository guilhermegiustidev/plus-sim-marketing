import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { CustomersComponent } from './customers.component';
import { SharedModule } from '../../../../core/modules/shared.module';



const routes = [

    {
        path     : 'customers',
        component: CustomersComponent,
    }
];

@NgModule({
    declarations: [
        CustomersComponent,
    ],
    imports     : [
        SharedModule,
        RouterModule.forChild(routes)
    ]
})

export class CustomersModule
{

}
