import { Component, OnInit } from '@angular/core'
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { locale as english } from './i18n/en';
import { locale as portuguese } from './i18n/pt';
import { locale as spanish } from './i18n/sp';

import { TranslateService } from '@ngx-translate/core';
import { TranslationLoader } from '../../../../core/services/translation-loader.service';
import { animations } from '../../../../core/animations';


declare var $: any;
declare var jquery: any;


@Component({
    templateUrl: './customers.component.html',
    styleUrls: ['./customers.component.scss'],
    animations: animations

})
export class CustomersComponent implements OnInit {
    
    formCustomer: FormGroup;
    formErrors: any;

    country: any;

    constructor(
        private translationLoader: TranslationLoader,
        private formBuilder: FormBuilder, 
        private translate: TranslateService
    ) {
        this.translationLoader.loadTranslations(english, portuguese, spanish);
        this.translate.use(localStorage.getItem('localeKey'));
        this.loadForms();
        this.loadFormsErrors();

    }
    
    onFormValuesChanged(): any {
        for (const field in this.formErrors) {
            if (!this.formErrors.hasOwnProperty(field)) {
                continue;
            }

            this.formErrors[field] = {};
            const control = this.formCustomer.get(field);

            if (control && control.dirty && !control.valid) {
                this.formErrors[field] = control.errors;
            }
        }
    }

    ngOnInit(): void {
        $("html, body").animate({ scrollTop: 0 }, "fast");
        this.formCustomer.valueChanges.subscribe(() => {
            this.onFormValuesChanged();
        });

        $('#js-grid-faq').cubeportfolio({
            filters: '#location-search',
            defaultFilter: '.list',
            animationType: 'sequentially',
            gridAdjustment: 'default',
            displayType: 'default',
            caption: 'expand',
            gapHorizontal: 0,
            gapVertical: 0
        });


    }

    private loadFormsErrors() {
        this.formErrors = {
            name: {},
        };
    }

    private loadForms() {
        this.formCustomer = this.formBuilder.group({
            name: ['', Validators.required],
        });
    }

}