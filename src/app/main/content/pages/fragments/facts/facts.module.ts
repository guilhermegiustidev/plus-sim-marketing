import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { FactComponent } from './facts.component';
import { SharedModule } from '../../../../../core/modules/shared.module';


@NgModule({
    declarations: [
        FactComponent
    ],
    exports: [
        FactComponent
    ],
    imports     : [
        SharedModule,
    ]
})

export class FactModule
{

}
