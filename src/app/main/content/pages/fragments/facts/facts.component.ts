import { Component, OnInit } from '@angular/core'

import { locale as english } from './i18n/en';
import { locale as portuguese } from './i18n/pt';
import { locale as spanish } from './i18n/sp';

import { TranslateService } from '@ngx-translate/core';
import { TranslationLoader } from '../../../../../core/services/translation-loader.service';

import { animations } from '../../../../../core/animations';

declare var $: any;
declare var jquery: any;


@Component({
    selector: 'fact',
    templateUrl: './facts.component.html',

})
export class FactComponent implements OnInit {

    costumersSize: number;
    hoursSold: number;
    facebookLikes: number;

    constructor(
        private translationLoader: TranslationLoader,
        private translate: TranslateService
    ) {
        this.translationLoader.loadTranslations(english, portuguese, spanish);
        this.translate.use(localStorage.getItem('localeKey'));
        this.costumersSize = 1234;
        this.hoursSold = 2345;
        this.facebookLikes = 3456;

    }

    ngOnInit(): void {
        $('#costumersSize').animateNumber({
            number: this.costumersSize,
            numberStep: function (now, tween) {
                var floored_number = Math.floor(now),
                    target = $(tween.elem);
                target.text(floored_number);
            }
        },
            10000
        );
        $('#hoursSold').animateNumber({
            number: this.hoursSold,
            numberStep: function (now, tween) {
                var floored_number = Math.floor(now),
                    target = $(tween.elem);
                target.text(floored_number);
            }
        },
            10000
        );
        $('#facebookLikes').animateNumber({
            number: this.facebookLikes,
            numberStep: function (now, tween) {
                var floored_number = Math.floor(now),
                    target = $(tween.elem);
                target.text(floored_number);
            }
        },
            10000
        )
    }


}