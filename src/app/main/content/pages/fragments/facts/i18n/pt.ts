export const locale = {
    lang: 'pt',
    data: {
        'LABELS': {
            'CUSTOMERS_SIZE': 'Representantes',
            'HOURS_SOLD': 'Horas de serviços prestados',
            'FACEBOOK_LIKES': 'Likes no facebook',
            'FACTS' : {
                'ABOUT_US' : {
                    'TITLE': 'Fatos sobre nós',
                    'DESC': 'TEXTO SOBRE FATORS SOBRE NOS',
                }
            }
        }
    }
};
