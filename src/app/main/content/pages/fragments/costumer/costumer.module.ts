import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { CostumerComponent } from './costumer.component';
import { SharedModule } from '../../../../../core/modules/shared.module';
import { ContactModule } from '../../customer-contanct/customer.contanct.module';

@NgModule({
    declarations: [
        CostumerComponent
    ],
    exports: [
        CostumerComponent
    ],
    imports     : [
        SharedModule,
        RouterModule,
        ContactModule
    ]
})

export class CostumerModule
{

}
