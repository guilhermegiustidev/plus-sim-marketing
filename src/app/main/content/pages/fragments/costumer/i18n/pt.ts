export const locale = {
    lang: 'pt',
    data: {
        'LABELS': {
            'WORK.WITH.US': {
                'TITLE': 'Fale conosco',
                'DESC': 'TEXTO CONTATENOS'
            }, 
            'CALL.US': 'Contate nos'
        }
    }
};
