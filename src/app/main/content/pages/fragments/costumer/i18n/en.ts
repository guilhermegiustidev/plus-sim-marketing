export const locale = {
    lang: 'en',
    data: {
        'LABELS': {
            'WORK.WITH.US': {
                'TITLE': 'Contact us',
                'DESC': 'Contact us'
            }, 
            'CALL.US': 'Call us'
        }
    }
};
