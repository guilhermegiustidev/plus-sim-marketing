import { Component, OnInit } from '@angular/core'

import { locale as english } from './i18n/en';
import { locale as portuguese } from './i18n/pt';
import { locale as spanish } from './i18n/sp';

import { TranslateService } from '@ngx-translate/core';
import { TranslationLoader } from '../../../../../core/services/translation-loader.service';

import { plans as Planos } from '../../../../../core/dto/plans';
import { animations } from '../../../../../core/animations';

declare var $: any;
declare var jquery: any;


@Component({
    selector: 'plans',
    templateUrl: './plans.component.html',

})
export class PlansComponent {

    param: any = {numberDays:10};

    plans: any[] = Planos;

    constructor(
        private translationLoader: TranslationLoader,
        private translate: TranslateService
    ) {
        this.translationLoader.loadTranslations(english, portuguese, spanish);
        this.translate.use(localStorage.getItem('localeKey'));

    }

    mouseenter(event){
        $(event.target).addClass('active');
    }

    mouseleave(event){
        $(event.target).removeClass('active');
    }


}