export const locale = {
    lang: 'sp',
    data: {
        'LABELS': {
            'HIRE': {
                'PLAN' : 'Contrate um plano',
                'KNOW_PLANS': 'Conheça nossos planos'
            },

            'DESCRIPTION': {
                'DESC01': 'Internet ilimitada com velocidade LTE/4G',
                'DESC02': 'Ligações ilimitadas para os Estados Unidos',
                'DESC03': 'Ligações ilimitadas para fixo e celulares de 75 países – incluindo Brasil (anexar link com lista de países)',
                'DESC04': 'SMS ilimitado',
                'DESC05': 'Número PlusSim brasileiro permite o recebimento de chamadas em seu PlusSim com tarifa de uma ligação local para quem realiza a ligação (DDD disponíveis: São Paulo e Rio de Janeiro) - Opcional',
                'DESC06': 'Siga-me direciona as ligações feitas para seu número de celular brasileiro enquanto você utiliza o PlusSim em sua viagem (favor consultar sua operadora para confirmação do serviço em seu plano) - Opcional ',
                'DESC07': 'Entre em contato conosco para verificar o melhor plano que irá atender suas necessidades (link para pagina de contato)',
                'DESC08': 'Adicione dias extras ao seu plano'
            },

            'DAYS': {
                'FIVE': {
                    'TITLE': '5 Dias',
                },
                'TEN': {
                    'TITLE': '10 Dias',
                },
                'FIFTEEN': {
                    'TITLE': '15 Dias',
                },
                'TWENTY': {
                    'TITLE': '20 Dias',
                },
                'TWENTY_FIVE': {
                    'TITLE': '25 Dias',
                },
                'THIRTY': {
                    'TITLE': '30 Dias',
                },
                'MORE.THAN.THIRTY': {
                    'TITLE': 'Acima de 30 Dias',
                },
                'EXTRA': {
                    'TITLE': 'Dias Extras',
                }
            }
        


        }
    }
};
