﻿export const locale = {
    lang: 'en',
    data: {
        'LABELS': {
            'HIRE': {
                'PLAN' : 'Buy a plan',
                'KNOW_PLANS': 'See ours plans'
            },

            'DESCRIPTION': {
                'DESC01': 'Unlimited LTE/4G internet',
                'DESC02': 'Unlimited calls in the US',
                'DESC03': 'Unlimited calls to cellphones and land lines to over 75 countries',
                'DESC04': 'Unlimited text messages',
                'DESC05': 'Contact us to get the best plan option that fits your needs',
                'DESC08': 'Add extra days to your plan'
            },

            'DAYS': {
                'FIVE': {
                    'TITLE': '5 Days',
                },
                'TEN': {
                    'TITLE': '10 Days',
                },
                'FIFTEEN': {
                    'TITLE': '15 Days',
                },
                'TWENTY': {
                    'TITLE': '20 Days',
                },
                'TWENTY_FIVE': {
                    'TITLE': '25 Days',
                },
                'THIRTY': {
                    'TITLE': '30 Days',
                },
                'MORE.THAN.THIRTY': {
                    'TITLE': 'Over 30 Days',
                },
                'EXTRA': {
                    'TITLE': 'Extra days',
                }
            }
        


        }
    }
};
