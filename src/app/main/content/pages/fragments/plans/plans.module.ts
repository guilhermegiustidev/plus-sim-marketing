import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { PlansComponent } from './plans.component';
import { SharedModule } from '../../../../../core/modules/shared.module';

@NgModule({
    declarations: [
        PlansComponent
    ],
    exports: [
        PlansComponent
    ],
    imports     : [
        SharedModule,
        RouterModule
    ]
})

export class PlanModule
{

}
