﻿export const locale = {
    lang: 'en',
    data: {
       'LABELS': {
            'SERVICES': {
                'TITLE': 'Services',

                'INTERNET' : {
                    'TITLE': 'Unlimited Internet LTE',
                    'DESCRIPTION': 'Unlimited internet access during the period of the plan'
                },

                'DEVIVERY' : {
                    'TITLE': 'Express Delivery',
                    'DESCRIPTION': 'GERAR TEXTO PARA ENTREGA EXPRESSA'
                },

                'UNLIMITED_CALL': {
                    'TITLE': 'Unlimited calls for more than 75 countries',
                    'DESCRIPTION': 'GERAR TEXTO PARA UNLIMITED_CALL'
                },
                'SMS' : {
                    'TITLE': 'Unlimited SMS',
                    'DESCRIPTION': 'GERAR TEXTO PARA SMS'
                },

                'RECEIVE': {
                    'TITLE': 'Make and receive calls from Brazil',
                    'DESCRIPTION': 'GERAR TEXTO PARA Faça e Receba'
                },

                'PLANS': {
                    'TITLE': 'Flexible Plans for your trip',
                    'DESCRIPTION': 'GERAR TEXTO PARA Planos flexíveis'
                },

                'COVER' :{
                    'TITLE': 'Biggest coverage in US',
                    'DESCRIPTION': 'GERAR TEXTO PARA Maior cobertura nos EUA'
                },

                'PLUS': {
                    'TITLE': 'Personal Hotspot',
                    'DESCRIPTION': 'GERAR TEXTO PARA Serviços diferenciado'
                }

            },
       }
    }
};
