export const locale = {
    lang: 'sp',
    data: {
       'LABELS': {
            'SERVICES': {
                'TITLE': 'Serviços',

                'INTERNET' : {
                    'TITLE': 'Internet Ilimitada LTE',
                    'DESCRIPTION': 'Acesso ilimitado á internet durante seu periodo de contratação'
                },

                'DEVIVERY' : {
                    'TITLE': 'Entrega Expressa',
                    'DESCRIPTION': 'GERAR TEXTO PARA ENTREGA EXPRESSA'
                },

                'UNLIMITED_CALL': {
                    'TITLE': 'Ligação ilimitada pra mais de 75 países',
                    'DESCRIPTION': 'GERAR TEXTO PARA UNLIMITED_CALL'
                },
                'SMS' : {
                    'TITLE': 'Sms ilimitado',
                    'DESCRIPTION': 'GERAR TEXTO PARA SMS'
                },

                'RECEIVE': {
                    'TITLE': 'Faça e Receba ligações do Brasil',
                    'DESCRIPTION': 'GERAR TEXTO PARA Faça e Receba'
                },

                'PLANS': {
                    'TITLE': 'Planos flexíveis para sua viagem',
                    'DESCRIPTION': 'GERAR TEXTO PARA Planos flexíveis'
                },

                'COVER' :{
                    'TITLE': 'Maior cobertura nos EUA',
                    'DESCRIPTION': 'GERAR TEXTO PARA Maior cobertura nos EUA'
                },

                'PLUS': {
                    'TITLE': 'Serviços diferenciado de roteamento',
                    'DESCRIPTION': 'GERAR TEXTO PARA Serviços diferenciado'
                }

            },
       }
    }
};
