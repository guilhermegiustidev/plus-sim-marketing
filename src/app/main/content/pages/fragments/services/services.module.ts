import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { ServicesComponent } from './services.component';
import { SharedModule } from '../../../../../core/modules/shared.module';


@NgModule({
    declarations: [
        ServicesComponent
    ],
    exports: [
        ServicesComponent
    ],
    imports     : [
        SharedModule,
    ]
})

export class ServiceModule
{

}
