export const locale = {
    lang: 'pt',
    data: {
        'LABELS': {
            'ABOUT_US_TITLE': 'Sobre nós',
            'ABOUT_US_TITLE_SUB': 'Conheça a Plus Sim',
            'WHO_WE_ARE': 'A PlusSim é uma empresa de varejo de comunicação que tem como objetivo manter nossos clientes conectados durante suas viagens internacionais com a melhor cobertura nos EUA. Todos os nossos planos oferecem acesso ilimitado à Internet LTE, chamadas ilimitadas e muitos outros serviços opcionais com entrega rápida onde quer que você esteja, nos EUA ou no Brasil. Então, vá em frente! Poste suas fotos, ligue para seus amigos e pesquise todos os lugares legais que você pode visitar durante sua estadia nos EUA. Nós estamos com você!'
        }
     }
};
