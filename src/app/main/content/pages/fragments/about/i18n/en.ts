﻿export const locale = {
    lang: 'en',
    data: {
        'LABELS': {
            'ABOUT_US_TITLE': 'About us',
            'ABOUT_US_TITLE_SUB': 'Know about PlusSim',
            'WHO_WE_ARE': 'PlusSim is a communication retail company that aims to keep our costumers connected during their international travels with the best coverage in the USA. All of our plans provide unlimited LTE internet, unlimited calls and many other optional services with fast delivery wherever you are in the US or Brazil. So, go ahead! Post your pictures, call your friends and research all the cool places you can visit during your stay in the US. We got you covered!'
        }
     }
};
