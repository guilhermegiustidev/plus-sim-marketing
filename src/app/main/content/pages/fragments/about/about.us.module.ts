import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { AboutUsComponent } from './about.us.component';
import { SharedModule } from '../../../../../core/modules/shared.module';


@NgModule({
    declarations: [
        AboutUsComponent
    ],
    exports: [
        AboutUsComponent
    ],
    imports     : [
        SharedModule,
    ]
})

export class AboutUsModule
{

}
