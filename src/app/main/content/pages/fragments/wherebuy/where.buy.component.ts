import { Component, OnInit } from '@angular/core'

import { locale as english } from './i18n/en';
import { locale as portuguese } from './i18n/pt';
import { locale as spanish } from './i18n/sp';

import { TranslateService } from '@ngx-translate/core';
import { TranslationLoader } from '../../../../../core/services/translation-loader.service';
import { Router } from '@angular/router';
import { animations } from '../../../../../core/animations';


declare var $: any;
declare var jquery: any;


@Component({
    selector: 'wherebuy',
    templateUrl: './where.buy.component.html',
    animations: animations

})
export class WhereBuyComponent {

    constructor(
        private translationLoader: TranslationLoader,
        private translate: TranslateService,
        private router: Router
    ) {
        this.translationLoader.loadTranslations(english, portuguese, spanish);
        this.translate.use(localStorage.getItem('localeKey'));

    }

    navigateToPlans(event){
        event.preventDefault();
        $("html, body").animate({ scrollTop: 0 }, "slow");
        setTimeout(() => {
            this.router.navigate(['/store']);
        }, 300);

    }


}