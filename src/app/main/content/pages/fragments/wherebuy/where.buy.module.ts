import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { WhereBuyComponent } from './where.buy.component';
import { SharedModule } from '../../../../../core/modules/shared.module';


@NgModule({
    declarations: [
        WhereBuyComponent
    ],
    exports: [
        WhereBuyComponent
    ],
    imports     : [
        RouterModule,
        SharedModule,
    ]
})

export class WhereBuyModule
{

}
