﻿export const locale = {
    lang: 'en',
    data: {
        'LABELS': {
            'BUY_ONLINE': {
                'TITLE': 'Buy Online',
            },
            'BUY_WITH_CUSTUMER': {
                'TITLE': 'See our Sales Agents',
            },
            'BUY_WITH_US': {
                'TITLE': 'Buy at our office',
            },
            'TITLE_WHERE': 'Where to buy',
        }
     }
};
