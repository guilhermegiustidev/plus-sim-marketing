export const locale = {
    lang: 'sp',
    data: {
        'LABELS': {
            'BUY_ONLINE': {
                'TITLE': 'Compre Online',
            },
            'BUY_WITH_CUSTUMER': {
                'TITLE': 'Veja nossos Representantes',
            },
            'BUY_WITH_US': {
                'TITLE': 'Compre em nossos escritório',
            },
            'TITLE_WHERE': 'Onde Comprar',
        }
     }
};
