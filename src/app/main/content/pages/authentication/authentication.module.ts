import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';


import { SharedModule } from '../../../../core/modules/shared.module';

import { WhereBuyModule } from '../fragments/wherebuy/where.buy.module';

const routes = [

    {
        path     : 'login',
        component: LoginComponent,
    }, {
       path: 'register',
       component: RegisterComponent 
    }
];

@NgModule({
    declarations: [
        LoginComponent,
        RegisterComponent
    ],
    imports     : [
        SharedModule,
        WhereBuyModule,
        RouterModule.forChild(routes)
    ]
})

export class AuthenticationModule
{

}
