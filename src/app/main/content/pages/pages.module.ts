import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { HomeModule } from './home/home.module';
import { FaqModule } from './faq/faq.module';
import { StoreModule } from './store/store.module';
import { SharedModule } from '../../../core/modules/shared.module';
import { TutorialSettings } from '../pages/dialog/tutorial.settings.component';
import { ComingSoonModule } from './coming/coming.soon.module';


@NgModule({
    imports: [
        HomeModule,
        FaqModule,
        StoreModule,
        SharedModule,
        ComingSoonModule
    ],

})

export class PagesModule { }
