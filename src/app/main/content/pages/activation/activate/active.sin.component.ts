import { Component, OnInit } from '@angular/core'
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';

import { TutorialSettings } from '../../dialog/tutorial.settings.component'

import { locale as english } from './i18n/en';
import { locale as portuguese } from './i18n/pt';
import { locale as spanish } from './i18n/sp';

import { TranslateService } from '@ngx-translate/core';
import { TranslationLoader } from '../../../../../core/services/translation-loader.service';
import { animations } from '../../../../../core/animations';


declare var $: any;
declare var jquery: any;


@Component({
    templateUrl: './active.sin.component.html',
    styleUrls: ['./active.sin.component.scss'],
    animations: animations

})
export class ActivationSinComponent implements OnInit {


    formActivation: FormGroup;
    formErrors: any;
    codigo: string;

    statusActivate: boolean = false;

    process: number = 0;

    constructor(
        private translationLoader: TranslationLoader,
        private formBuilder: FormBuilder,
        private translate: TranslateService,
        public dialog: MatDialog
    ) {
        this.translationLoader.loadTranslations(english, portuguese, spanish);
        this.translate.use(localStorage.getItem('localeKey'));
        this.loadForms();
        this.loadFormsErrors();
    }

    openDialog(event): void {
        event.preventDefault();
        let dialogRef = this.dialog.open(TutorialSettings, {

        });
    
      }
    

    onFormValuesChanged(): any {
        for (const field in this.formErrors) {
            if (!this.formErrors.hasOwnProperty(field)) {
                continue;
            }

            this.formErrors[field] = {};
            const control = this.formActivation.get(field);

            if (control && control.dirty && !control.valid) {
                this.formErrors[field] = control.errors;
            }
        }
    }

    ngOnInit(): void {
        $("html, body").animate({ scrollTop: 0 }, "fast");
        this.formActivation.valueChanges.subscribe(() => {
            this.onFormValuesChanged();
        });
    }

    activate(event) {
        event.preventDefault();
        if (!this.statusActivate) {
            this.statusActivate = true;
            this.activateService();
        }
    }

    activateService() {
        if(this.process < 100){
            setTimeout(() => {
                this.process++;
                this.activateService();
            }, 100);
        } else {
            this.statusActivate = false;
            this.process = 0;
        }
    }

    private loadFormsErrors() {
        this.formErrors = {
            codigo: {},
        };
    }

    private loadForms() {
        this.formActivation = this.formBuilder.group({
            codigo: ['', Validators.required],
        });
    }

}