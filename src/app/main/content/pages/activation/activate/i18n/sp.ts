export const locale = {
    lang: 'sp',
    data: {
       'LABELS': {
           'PROGRESS': 'Progresso',
           'TEXT': {
                'ACTIVATION': {
                    'CONFIRM': 'Iniciar Ativação',
                    'BAR_CODE': 'Código de barras',
                    'BAR_CODE_HOLDER': 'Insira o código de barras',
                    'EXPLAIN': 'Antes ativar o seu chip da PlusSim, favor seguir o verifique o',
                    'CONTACT_US': 'Caso tenha alguma dúvida ou dificuldade, entre em contato conosco 1-800-555-5555)',
                    'INPUT': 'Insira o número de seu código de barras (ICCID) – (imagem – onde está localizado o código de barras)'
                },
                'TUTORIAL': 'passo à passo',
           },
            'OPTIONS': {

            }
       }
    }
};
