﻿export const locale = {
    lang: 'en',
    data: {
       'LABELS': {
           'PROGRESS': 'Progress',
           'TEXT': {
                'ACTIVATION': {
                    'CONFIRM': 'Start Activation',
                    'BAR_CODE': 'Barcode',
                    'BAR_CODE_HOLDER': 'Insert the barcode',
                    'EXPLAIN': 'Antes ativar o seu chip da PlusSim, favor seguir o verifique o',
                    'CONTACT_US': 'If you have any difficulties or questions, please contact us at +1 (321) 310-4764',
                    'INPUT': 'Insert your barcode (ICCID)'
                },
                'TUTORIAL': 'Step by step',
           },
            'OPTIONS': {

            }
       }
    }
};
