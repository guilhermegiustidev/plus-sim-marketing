﻿export const locale = {
    lang: 'en',
    data: {
       'LABELS': {
            'WHERE_BUY_TITLE': 'Buy your Plus Sim',
            'WHERE_BUY_TITLE_SUB': 'SUB_TUITULO',
            'CHECK_ACTIVATION': 'Activation check',
            'CHOOSE_BUY_PAGE': 'Where to buy'
       }
    }
};
