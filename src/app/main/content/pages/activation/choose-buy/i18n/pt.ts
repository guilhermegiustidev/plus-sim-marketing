export const locale = {
    lang: 'pt',
    data: {
       'LABELS': {
            'WHERE_BUY_TITLE': 'Adiquira seu Plus sim',
            'WHERE_BUY_TITLE_SUB': 'SUB_TUITULO',
            'CHECK_ACTIVATION': 'Verificação de ativação',
            'CHOOSE_BUY_PAGE': 'Escolha onde comprar'
       }
    }
};
