import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { CheckActivationComponent } from './check-step/check.activation.component';
import { ChooseBuyComponent } from './choose-buy/choose.buy.component';
import { SharedModule } from '../../../../core/modules/shared.module';

import { ActivationSinComponent } from './activate/active.sin.component';
import { WhereBuyModule } from '../fragments/wherebuy/where.buy.module';
import { Component } from '@angular/core/src/metadata/directives';
import { TutorialSettings } from '../../pages/dialog/tutorial.settings.component';

const routes = [

    {
        path        : 'checkActivation',
        component   : CheckActivationComponent,
    }, {
       path         : 'chooseBuy',
       component    : ChooseBuyComponent 
    }, {
        path        : 'activation', 
        component   : ActivationSinComponent
    }
];

@NgModule({
    declarations: [
        CheckActivationComponent,
        ChooseBuyComponent,
        ActivationSinComponent,
        TutorialSettings
    ],
    imports     : [
        SharedModule,
        WhereBuyModule,
        RouterModule.forChild(routes)
    ],
    entryComponents: [
        TutorialSettings
    ]
})

export class ActivationModule
{

}
