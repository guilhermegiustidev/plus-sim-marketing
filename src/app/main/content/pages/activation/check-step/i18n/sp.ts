export const locale = {
    lang: 'sp',
    data: {
       'LABELS': {
            'ACTIVATE': 'Ativando Chip',
            'ACTIVATE_HEADER_TEXT': 'Siga os passos para ativação',
            'CHECK_ACTIVATION': 'Ativação',
            'OPTIONS': {
                'BUY': 'Comprar Chip',
                'ACTIVATE': 'Ativar Chip',
                'BUY_TEXT': 'Texto Comprar',
                'ACTIVATE_TEXT': 'Texto Ativar',
                
            }
       }
    }
};
