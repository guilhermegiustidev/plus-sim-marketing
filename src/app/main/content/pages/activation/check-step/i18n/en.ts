﻿export const locale = {
    lang: 'en',
    data: {
       'LABELS': {
            'ACTIVATE': 'Activating Sim Card',
            'ACTIVATE_HEADER_TEXT': 'Follow the Instructions to activate your Sim Card',
            'CHECK_ACTIVATION': 'Activation',
            'OPTIONS': {
                'BUY': 'Buy',
                'ACTIVATE': 'Activate',
                'BUY_TEXT': 'Buy',
                'ACTIVATE_TEXT': 'Activate',
                
            }
       }
    }
};
