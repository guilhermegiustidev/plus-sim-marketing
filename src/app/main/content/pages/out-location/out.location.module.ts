import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { OutLocationComponent } from './out.location.component';
import { SharedModule } from '../../../../core/modules/shared.module';



const routes = [

    {
        path     : 'ourlocation',
        component: OutLocationComponent,
    }
];

@NgModule({
    declarations: [
        OutLocationComponent,
    ],
    imports     : [
        SharedModule,
        RouterModule.forChild(routes)
    ]
})

export class OutLocationModule
{

}
