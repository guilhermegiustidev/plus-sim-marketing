export const locale = {
    lang: 'pt',
    data: {
       'LABELS': {
            'PAGE' : 'Nosso endereço',
            'HEADER': {
                'OUR_LOCATION': 'Nosso escritório',           
                'OUR_LOCATION_SUB': 'SUB_TEXTO'
            }
       }
    }
};
