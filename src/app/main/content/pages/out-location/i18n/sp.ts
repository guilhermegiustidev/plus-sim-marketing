export const locale = {
    lang: 'sp',
    data: {
       'LABELS': {
            'PAGE' : 'Nosso endereço',
            'HEADER': {
                'OUR_LOCATION': 'Nosso escritório',           
                'OUR_LOCATION_SUB': 'SUB_TEXTO'
            }
       }
    }
};
