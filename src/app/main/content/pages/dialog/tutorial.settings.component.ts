import { Component, Inject } from '@angular/core'

import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';


import { locale as english } from './i18n/en';
import { locale as portuguese } from './i18n/pt';
import { locale as spanish } from './i18n/sp';

import { TranslateService } from '@ngx-translate/core';
import { TranslationLoader } from '../../../../core/services/translation-loader.service';
import { animations } from '../../../../core/animations';


declare var $: any;
declare var jquery: any;


@Component({
    templateUrl: './tutorial.settings.component.html',
    styleUrls: ['./tutorial.settings.component.scss'],
    animations: animations

})
export class TutorialSettings {
    
    constructor(
        private translationLoader: TranslationLoader,
        private translate: TranslateService,
        public dialogRef: MatDialogRef<TutorialSettings>
    ) {
        this.translationLoader.loadTranslations(english, portuguese, spanish);
        this.translate.use(localStorage.getItem('localeKey'));
    }
    
    ngOnInit(): void {

    }
    
}