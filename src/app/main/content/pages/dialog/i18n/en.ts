﻿export const locale = {
    lang: 'en',
    data: {
       'LABELS': {
         'HEADER_TUTORIAL': 'Tutorial',
         'HEADER_TUTORIAL_SUB_TITLE': 'To start using your Plus Sim:',
         'LIST':{
             'POINT': {
                '0': 'check if your phone is unlocked and works with others international carriers',
                '1': 'Check which Sim Card size is compatible with your cellphone before detach it from the plastic card.',
                '2': 'After insert the Sim Card on your cellphone, turn on "Data"  and "Roaming" on the setting “Cellular Data”.',
                '3': 'To the cellphones that support two Sim Cards, insert your Plus Sim in the first slot and remove any Sim Card from the second slot.',
                '4': 'Your Cellphone will get the settings automatically. If you receive the message “Carrier Settings Update”, press “update”. You do not need to follow the instructions received by SMS. ',
                '5': 'Settings > Wireless and Network > More > Mobile Networks > Acess Point Name > Add Access point ( "+" up right) >  Name: Ultra Mobile / APPN: Wholesale / MMSC: http://wholesale.mmsmvno.com/mms/wapenc  / MCC310: MNC260 > Save and select the new Access Point',
            
            },
        },
        'FOOTER_TEXT': 'Caso tenha alguma dúvida ou dificuldade, entre em contato conosco 1-800-555-5555 ou clique aqui para falar no chat com a nossa área de suporte. Atenção: Algumas versões do aparelho MotoG não são compativeis com a rede 4G dos Estados Unidos.  '
       }
    }
};
