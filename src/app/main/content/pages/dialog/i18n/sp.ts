export const locale = {
    lang: 'sp',
    data: {
       'LABELS': {
         'HEADER_TUTORIAL': 'Passo à passo',
         'HEADER_TUTORIAL_SUB_TITLE': 'Para começar a utilizar o seu PlusSim:',
         'LIST':{
             'POINT': {
                '0': 'Verifique se o seu aparelho é desbloqueado e funciona com chip de diferentes países.',
                '1': 'Verifique qual é o tamanho do chip compatível com seu aparelho ANTES de destacar do cartão plástico. ',
                '2': 'Após inserir o chip em seu celular, ative os dados moveis e o Roaming na opção “Dados Moveis”. ',
                '3': 'Para celulares que suportam dois chips, colocar o PlusSim na posição número 1 e deixar a posição número 2 livre de outro chip. ',
                '4': 'o seu aparelho irá se configurar automaticamente. Caso você receba uma mensagem para “Atualização dos ajustes da operadora” aperte “OK”. Você não precisar seguir as recomendações recebidas por SMS. ',
                '5': 'Configuração > Redes > Mais > Redes Moveis > Nome de Pontos de Acesso > Adicionar um Ponto de Acesso (canto superior direito) >  Nome: Ultra Mobile / APPN: Wholesale / MMSC: http://wholesale.mmsmvno.com/mms/wapenc  / MCC310: MNC260 > Salvar e Selecionar o novo ponto de acesso',
            
            },
        },
        'FOOTER_TEXT': 'Caso tenha alguma dúvida ou dificuldade, entre em contato conosco 1-800-555-5555 ou clique aqui para falar no chat com a nossa área de suporte. Atenção: Algumas versões do aparelho MotoG não são compativeis com a rede 4G dos Estados Unidos.  '
       }
    }
};
