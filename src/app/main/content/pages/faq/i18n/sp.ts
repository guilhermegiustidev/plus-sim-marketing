export const locale = {
    lang: 'sp',
    data: {
       'LABELS': {
            'FAQs': {
                'TITLE': 'FAQs',
                'SUB_TITLE': 'Perguntas Frequentes',
                'TOPICS': {
                    'TITLE':{
                        '0': 'Como comprar e em quais países o chip funciona?',
                        '1': 'Como faço ligações para outros países?',
                        '2': 'O que é Siga-me?',
                        '3': 'Como funciona o Siga-me? ',
                        '4': 'Como realizar a configuração do Siga-me?',
                        '5': 'Como e para que funciona o Número PlusSim Brasileiro?',
                        '6': 'Consigo manter meu número brasileiro no aplicativo “Whatsapp”?',
                        '7': 'Quais as opções de entrega?',
                        '8': 'Preciso realizar alguma configuração no aparelho?',
                        '9': 'Quais são os países incluídos para realizar ligações ilimitadas?',
                        '10': 'Quais as formas de pagamento?',
                        '11': 'Como sei que meu chip está ativado?',
                        '12': 'Posso alterar o período da minha viagem?',
                        '13': 'Posso cancelar a compra?',
                        '14': 'Como faço a extensão da minha viagem?',
                        '15': 'Posso reutilizar o chip em outras viagens?',
                        '16': 'Meu chip não está funcionando. Quais são as formas de contato para suporte?',

                    },
                    'BODY': {
                        '0': 'Para comprar seu PlusSim, clique aqui e conheça nossos planos. A PlusSim oferece cobertura em todo o território dos Estados Unidos. ',
                        '1': 'Para fazer ligações para outros países, basta discar + (segurando o número zero) (código do país) (código do estado) (número do telefone). Por exemplo, + 55 11 xxxx-xxxx.',
                        '2': 'O serviço Siga-me direciona as ligações feitas para seu número de celular brasileiro enquanto você utiliza o PlusSim em sua viagem.  Antes de contratar esse serviço, certifique-se junto a sua operadora do Brasil (Vivo, Tim, Oi, Claro, Nextel e etc) se seu plano possui disponibilidade para esse serviço.',
                        '3': 'O Siga-me e um serviço gratuito oferecido pelas operadoras do Brasil, porém, para que ele possa funcionar nos Estados Unidos, e necessário a criação de um Numero PlusSim Brasileiro com custo de US$ 5,00. Esse número será enviado por e-mail até 1 dia antes do seu embarque, junto com as instruções para ativação do serviço. ',
                        '4': 'Para ativar o Siga-me, primeiramente, você deve consultar com sua operadora do Brasil (Vivo, Tim, Oi, Claro, Nextel e etc) se seu plano possui disponibilidade para esse serviço. Uma vez confirmada essa informação e solicitado o serviço do Siga-me, siga o procedimento abaixo com o seu chip do Brasil ainda em seu aparelho antes de sair do país: Identifique no e-mail de ativação recebido até 1 dia antes do seu embarque, o seu Numero PlusSim Brasileiro. Na tela de ligações do seu celular, disque os códigos abaixo: Vivo, Claro, Oi e Nextel - *21* Número PlusSim Brasileiro # TIM - **21*041 Número PlusSim Brasileiro # Após seguir os passos acima, você deverá receber uma mensagem informando que o Siga-me foi realizado com sucesso. Caso não receba essa mensagem, entre em contato com nosso suporte. (inserir link para Contact us)',
                        '5': 'O número PlusSim Brasileiro possibilita o recebimento de chamadas em seu PlusSim com tarifa de uma ligação local para quem realiza a ligação (DDD disponíveis: São Paulo e Rio de Janeiro).',
                        '6': 'Sim, na primeira vez que você acessar o Whatsapp após inserir o PlusSim, o próprio aplicativo ira perguntar se você deseja substituir seu número por um novo ou se você deseja manter seu número. Basta clicar em “Manter Meu Número” para continuar com seu próprio número de telefone.',
                        '7': 'Oferecemos entregas via correio no território do Brasil e nos Estados Unidos. Na cidade de Orlando, realizamos entregas expressas ou disponibilidade de retirada em custo em nosso escritório.',
                        '8': 'Para configurar o seu aparelho: iOS/Apple: o seu aparelho irá se configurar automaticamente. Caso você receba uma mensagem para “Atualização dos ajustes da operadora” aperte “OK”. Você não precisar seguir as recomendações recebidas por SMS. Android: Configuração > Redes > Mais > Redes Moveis > Nome de Pontos de Acesso > Adicionar um Ponto de Acesso (canto superior direito) >  Nome: Ultra Mobile / APPN: Wholesale / MMSC: http://wholesale.mmsmvno.com/mms/wapenc  / MCC310: MNC260 > Salvar e Selecionar o novo ponto de acesso Caso tenha alguma dúvida ou dificuldade, entre em contato conosco 1-800-555-5555 ou clique aqui para falar no chat com a nossa área de suporte.',
                        '9': 'Em breve mais informações',
                        '10': 'Pagamento seguro em nosso site via cartão de credito ou pelo PayPal.',
                        '11': 'Ate 1 dia antes de seu embarque, você irá receber em seu e-mail a confirmação de ativação do seu chip. Caso não tenha recebido, favor entrar em contato com o nosso suporte (colocar o link de Contact us). ',
                        '12': 'Sim, o período da viagem pode ser alterado sem custo até 2 dias antes da data do embarque anterior. Caso o chip já tenha sido ativado e o e-mail de ativação enviado, será cobrada uma taxa de USD $5,00 para remarcação mais o custo de envio de um novo chip.',
                        '13': 'Para o cancelamento de sua compra, favor entrar em contato com o nosso suporte (colocar o link de Contact us).',
                        '14': 'Favor entrar em contato com nosso suporte antes do termino da sua viagem.',
                        '15': 'Infelizmente, o chip não pode ser reutilizado em outras viagens.',
                        '16': 'Clique aqui para saber as formas de contato.',

                    }
                }

            }
       }
    }
};
