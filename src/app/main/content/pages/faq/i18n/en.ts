﻿export const locale = {
    lang: 'en',
    data: {
       'LABELS': {
            'FAQs': {
                'TITLE': 'FAQs',
                'SUB_TITLE': 'Frequent Asked Questions',
                'TOPICS': {
                    'TITLE':{
                        '0': 'How to buy, and what are the countries that the Sim Card works? ',
                        '1': 'How do I make international calls?',
                        '2': 'Can I keep my own number on the app Whatsapp?',
                        '3': 'What are the Plus Sim delivery options?',
                        '4': 'Do I need to change any configuration on the Cellphone?',
                        '5': 'What are the countries that I can call unlimited?',
                        '6': 'What are the payment options?',
                        '7': 'How do I know that my Sim Card was activated?',
                        '8': 'Can I change the travel period?',
                        '9': 'Can I cancel the purchace?',
                        '10': 'How do I extend my period of stay?',
                        '11': 'Can I use the same Sim Card on my next trip?',
                        '12': 'My Sim Card is not working. How can I contact the support?',
                        '13': 'Posso cancelar a compra?',
                        '14': 'Como faço a extensão da minha viagem?',
                        '15': 'Posso reutilizar o chip em outras viagens?',
                        '16': 'Meu chip não está funcionando. Quais são as formas de contato para suporte?',

                    },
                    'BODY': {
                        '0': 'To buy your Plus Sim, click here and know our plans. Plus Sim offers you coverage in all US territory.',
                        '1': 'To make international calls, dial + (press and hold number zero) (Country code) (state/local code) (phone number). For Exemple: + 55 11 xxxx-xxxx.',
                        '2': 'Yes, once you access the app after using your PlusSim, you will need to select if you want to keep you number or change it for a new number. Please select that you want to keep your number.',
                        '3': 'We deliver by mail in US or Brazil. In Orlando, we offer an express delivery or free pick up in our office.',
                        '4': 'To configurate your device: iOS/Apple: your device should configurate itself automatically. In case you receive a message to accept the Carriers Updates, press OK. You do not need to follow the steps received by SMS. Android: Go to Configurations > Networks > More > Cellular Data > Access point name > Add a access point  (top right corner) >  Name: Ultra Mobile / APPN: Wholesale / MMSC: http://wholesale.mmsmvno.com/mms/wapenc  / MCC310: MNC260 > Save and select the new access point.If you are experiencing problems or having any doubts regarding your PlusSim, please contact us at +1 (321) 310-4764.',
                        '5': 'You can get refund until 30 days after the purchase. The shipping fee is not included in the refund.',
                        '6': 'Secure online card payment thru our website or using PayPal.',
                        '7': 'Until 1 day before your trip, you will receive a confirmation by e-mail. If you did not receive it, please contact our support.',
                        '8': 'Yes, the travel period can be changed withou fee BEFORE the activation is made (you will receive a confirmation by e-mail) which means 2 days before your trip.',
                        '9': 'To get a refund on your PlusSim, you need to cancel the purchase until 30 days after buying it. The delivery fee is not included in the refund, and there is a $5.00 fee.',
                        '10': 'Please contact our team before the end of your trip',
                        '11': 'Unfortunately, the simcard cannot be reactivate.',
                        '12': 'Click here to see our contact information.',
                        '13': 'Para o cancelamento de sua compra, favor entrar em contato com o nosso suporte (colocar o link de Contact us).',
                        '14': 'Favor entrar em contato com nosso suporte antes do termino da sua viagem.',
                        '15': 'Infelizmente, o chip não pode ser reutilizado em outras viagens.',
                        '16': 'Clique aqui para saber as formas de contato.',

                    }
                }

            }
       }
    }
};
