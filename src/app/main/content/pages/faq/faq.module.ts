import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { FaqComponent } from './faq.component';
import { SharedModule } from '../../../../core/modules/shared.module';



const routes = [

    {
        path     : 'faq',
        component: FaqComponent,
    }
];

@NgModule({
    declarations: [
        FaqComponent,
    ],
    imports     : [
        SharedModule,
        RouterModule.forChild(routes)
    ]
})

export class FaqModule
{

}
