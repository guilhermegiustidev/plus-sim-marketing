﻿export const locale = {
    lang: 'en',
    data: {
       'LABELS': {
           'FIRST_TEXT': 'Await further information',
           'SECOND_TEXT': 'Coming Soon'
       }
    }
};
