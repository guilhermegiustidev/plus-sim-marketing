export const locale = {
    lang: 'pt',
    data: {
       'LABELS': {
           'FIRST_TEXT': 'Aguarde para mais informações',
           'SECOND_TEXT': 'Em Breve'
       }
    }
};
