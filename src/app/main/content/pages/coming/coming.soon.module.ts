import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { ComingSoonComponent } from './coming.soon.component';
import { SharedModule } from '../../../../core/modules/shared.module';



const routes = [

    {
        path     : 'comingSoon',
        component: ComingSoonComponent,
    }
];

@NgModule({
    declarations: [
        ComingSoonComponent,
    ],
    imports     : [
        SharedModule,
        RouterModule.forChild(routes)
    ]
})

export class ComingSoonModule
{

}
