import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { SharedModule } from '../../../../core/modules/shared.module';

import { WhereBuyModule } from '../fragments/wherebuy/where.buy.module';
import { Component } from '@angular/core/src/metadata/directives';

import { ViewComponent } from './view/view.component';
import { CheckOutComponent } from './checkout/checkout.component';


const routes = [

    {
        path        : 'cart',
        component   : ViewComponent,
    }, {
       path         : 'checkout',
       component    : CheckOutComponent 
    }
];

@NgModule({
    declarations: [
        ViewComponent,
        CheckOutComponent
    ],
    imports     : [
        SharedModule,
        WhereBuyModule,
        RouterModule.forChild(routes)
    ],
})

export class CartModule
{

}
