import { Component, OnInit } from '@angular/core'
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';

import { TutorialSettings } from '../../dialog/tutorial.settings.component'

import { locale as english } from './i18n/en';
import { locale as portuguese } from './i18n/pt';
import { locale as spanish } from './i18n/sp';

import { TranslateService } from '@ngx-translate/core';
import { TranslationLoader } from '../../../../../core/services/translation-loader.service';
import { animations } from '../../../../../core/animations';


declare var $: any;
declare var jquery: any;


@Component({
    templateUrl: './checkout.component.html',
    styleUrls: ['./checkout.component.scss'],
    animations: animations

})
export class CheckOutComponent implements OnInit {

    constructor(
        private translationLoader: TranslationLoader,
        private formBuilder: FormBuilder,
        private translate: TranslateService,
        public dialog: MatDialog
    ) {
        this.translationLoader.loadTranslations(english, portuguese, spanish);
        this.translate.use(localStorage.getItem('localeKey'));
    }
    ngOnInit(): void {
        $("html, body").animate({ scrollTop: 0 }, "fast");
    }

}