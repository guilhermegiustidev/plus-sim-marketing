import { Component, OnInit } from '@angular/core'

import { locale as english } from './i18n/en';
import { locale as portuguese } from './i18n/pt';
import { locale as spanish } from './i18n/sp';

import { TranslateService } from '@ngx-translate/core';
import { TranslationLoader } from '../../../../core/services/translation-loader.service';
import { animations } from '../../../../core/animations';


declare var $: any;
declare var jquery: any;


@Component({
    templateUrl: './customer.contanct.component.html',
    styleUrls: ['./customer.contanct.component.scss'],
    animations: animations

})
export class CustomerContact implements OnInit {
    
    constructor(
        private translationLoader: TranslationLoader,
        private translate: TranslateService
    ) {
        this.translationLoader.loadTranslations(english, portuguese, spanish);
        this.translate.use(localStorage.getItem('localeKey'));
    }

    
    ngOnInit(): void {
        $("html, body").animate({ scrollTop: 0 }, "fast");
    }
}