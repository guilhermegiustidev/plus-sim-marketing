import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { CustomerContact } from './customer.contanct.component';
import { SharedModule } from '../../../../core/modules/shared.module';

const routes = [

    {
        path     : 'concact',
        component: CustomerContact,
    }
];

@NgModule({
    declarations: [
        CustomerContact,
    ],
    imports     : [
        SharedModule,
        RouterModule.forChild(routes)
    ]
})

export class ContactModule
{

}
