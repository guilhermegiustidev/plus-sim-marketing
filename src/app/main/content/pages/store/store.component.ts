import { Component, OnInit } from '@angular/core'
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';

import { locale as english } from './i18n/en';
import { locale as portuguese } from './i18n/pt';
import { locale as spanish } from './i18n/sp';

import { TranslateService } from '@ngx-translate/core';
import { TranslationLoader } from '../../../../core/services/translation-loader.service';
import { animations } from '../../../../core/animations';

import { plans } from '../../../../core/dto/plans';

declare var $: any;
declare var jquery: any;


@Component({
    templateUrl: './store.component.html',
    styleUrls: ['./store.component.scss'],
    animations: animations

})
export class StoreComponent implements OnInit {

    plans: Array<any>;
    selectedPlan: any = {};

    choosePlan: FormGroup;
    formErrors: any;
    quantityDays: number;

    showCustomDays: boolean;
    contactUs: boolean;

    custom: any;
    moreThan: any;

    constructor(
        private translationLoader: TranslationLoader,
        private formBuilder: FormBuilder, 
        private translate: TranslateService
    ) {
        this.plans = new Array<any>();
        for(let index of plans){
            if(index.days){
                this.plans.push(index);
            }
        }
        this.translationLoader.loadTranslations(english, portuguese, spanish);
        this.translate.use(localStorage.getItem('localeKey'));
        this.loadForms();
        this.loadFormsErrors();
        this.moreThan = {
            id: "9", 
            title: "LABELS.DAYS.MORE.THAN.THIRTY", 
        }
        this.custom = {
            id: "10", 
            title: "LABELS.DAYS.CUSTOM", 
        }        
    }
    
    ngOnInit(): void {
        $("html, body").animate({ scrollTop: 0 }, "fast");
        $('#js-grid-faq').cubeportfolio({
            filters: '#location-search',
            defaultFilter: '.specs',
            animationType: 'sequentially',
            gridAdjustment: 'default',
            displayType: 'default',
            caption: 'expand',
            gapHorizontal: 0,
            gapVertical: 0
        });

    }


    select(event){
        if(parseInt(event.value.id) === 10){
            this.showCustomDays = true;
        } else {
            this.showCustomDays = false;
        }

        if(parseInt(event.value) === 0){
            this.contactUs = true;
        } else {
            this.contactUs = false;
        }
    }
    
    addOnCart(event){
        event.preventDefault();
    }

    changeDays(event){
        if(!this.quantityDays){
            this.quantityDays = 5;
        }

        if(this.quantityDays < 5){
            this.quantityDays = 5;
        }

        if(this.quantityDays > 30){
            this.quantityDays = 30;
        }

    }

    private loadFormsErrors() {
        this.formErrors = {
            country: {},
            plan: {},
            quantityDays: {}
        };
    }

    private loadForms() {
        this.choosePlan = new FormGroup({
            plan: new FormControl('', 
            Validators.compose([ Validators.required ])),
                
            quantityDays: new FormControl('', 
              Validators.compose([ Validators.minLength(5), Validators.maxLength(30) ])),
          });
    }
}