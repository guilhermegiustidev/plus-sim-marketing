import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { StoreComponent } from './store.component';
import { SharedModule } from '../../../../core/modules/shared.module';



const routes = [

    {
        path     : 'store',
        component: StoreComponent,
    }
];

@NgModule({
    declarations: [
        StoreComponent,
    ],
    imports     : [
        SharedModule,
        RouterModule.forChild(routes)
    ]
})

export class StoreModule
{

}
