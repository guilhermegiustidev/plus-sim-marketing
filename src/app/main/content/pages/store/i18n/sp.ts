export const locale = {
    lang: 'sp',
    data: {
        'LABELS': {

            'TITLE': {
                'STORE': 'PlusSim Ilimitado para os Estados Unidos'
            },

            'HEADER': {
                'TITLE': 'Nossa Loja',
                'TITLE_SUB': 'Compre online seu plussim'
            },

            'HOW_TO_ACTIVE_DESC_TITLE': 'Como ativar seu PlusSim',
            'HOW_TO_ACTIVE_DESC_SUB': 'Siga os passos para ativar seu chip',
            'MORE_THAN_30_CONTANCT': 'Para mais de 30 dias entrar em contato',
            'MORE_THAN_30_BUTTON': 'Contate nos',
            'ADD_TO_CART': 'Adicionar ao Carrinho',
            'DAYS_NUMBER': 'Número de dias',
            'CONFIRM': 'Confirmar',
            'BUILD.PLAN': 'Monte um seu plano',
            'CUSTOM.DAYS': 'Informe um valor de dias diferente',
            'SELECT.DATA.PLAN': 'Detalhes do plano Selecionado',
            'PLANS': 'Nossos Planos',
            'TEXT': {
                'COMPRA_ONLINE': 'Monte seu plano, escolha o prazo ideal para a sua viagem',
                'STORE': 'Texto para compra do Chip, BlaBlablaBlaBlablaBlaBlablaBlaBlablaBlaBlabla'
            },
            'TAB_STORE': {
                'HOW_TO_ACTIVE' : 'Como Ativar',
                'ADVANTAGES': 'Vantagens'
            },
            'DAYS': {
                'FIVE': '5 Dias',
                'TEN': '10 Dias',
                'FIFTEEN': '15 Dias',
                'TWENTY': '20 Dias',
                'TWENTY_FIVE': '25 Dias',
                'THIRTY': '30 Dias',
                'MORE.THAN.THIRTY': 'Acima de 30 Dias',
                'EXTRA': 'Dias Extras',
                'CUSTOM': 'Período customizado',
                'CUSTOM_QTD': 'Quantidade de dias'
            },
            'DESCRIPTION': {
                'DESC01': 'Internet ilimitada com velocidade LTE/4G',
                'DESC02': 'Ligações ilimitadas para os Estados Unidos',
                'DESC03': 'Ligações ilimitadas para fixo e celulares de 75 países – incluindo Brasil (anexar link com lista de países)',
                'DESC04': 'SMS ilimitado',
                'DESC05': 'Número PlusSim brasileiro permite o recebimento de chamadas em seu PlusSim com tarifa de uma ligação local para quem realiza a ligação (DDD disponíveis: São Paulo e Rio de Janeiro) - Opcional',
                'DESC06': 'Siga-me direciona as ligações feitas para seu número de celular brasileiro enquanto você utiliza o PlusSim em sua viagem (favor consultar sua operadora para confirmação do serviço em seu plano) - Opcional ',
                'DESC07': 'Entre em contato conosco para verificar o melhor plano que irá atender suas necessidades (link para pagina de contato)',
                'DESC08': 'Adicione dias extras ao seu plano'
            }
        },

    }
};
