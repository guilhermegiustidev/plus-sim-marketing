﻿export const locale = {
    lang: 'en',
    data: {
        'LABELS': {

            'TITLE': {
                'STORE': 'Unlimited PlusSim to the United States'
            },

            'HEADER': {
                'TITLE': 'Our store',
                'TITLE_SUB': 'Buy online'
            },

            'HOW_TO_ACTIVE_DESC_TITLE': 'How to activate your PlusSim',
            'HOW_TO_ACTIVE_DESC_SUB': 'Follow the steps to activate your PlusSim simcard',
            'MORE_THAN_30_CONTANCT': 'Over 30 days, please contact us to get the best plan option that fits your needs',
            'MORE_THAN_30_BUTTON': 'Contact us',
            'ADD_TO_CART': 'Add to cart',
            'DAYS_NUMBER': 'Number of days',
            'CONFIRM': 'Confirm',
            'BUILD.PLAN': 'Build a custom plan',
            'CUSTOM.DAYS': 'Inform a different number of days',
            'SELECT.DATA.PLAN': 'Details of the selected plan',
            'PLANS': 'Our Plans',
            'TEXT': {
                'COMPRA_ONLINE': 'Build a custom plan, choose the perfect duration for your trip',
                'STORE': 'Have unlimited coverage during your trip'
            },
            'TAB_STORE': {
                'HOW_TO_ACTIVE' : 'How to activate your PlusSim',
                'ADVANTAGES': 'Advantages'
            },
            'DAYS': {
                'FIVE': '5 Days',
                'TEN': '10 Days',
                'FIFTEEN': '15 Days',
                'TWENTY': '20 Days',
                'TWENTY_FIVE': '25 Days',
                'THIRTY': '30 Days',
                'MORE.THAN.THIRTY': 'Over 30 Days',
                'EXTRA': 'Extra Day',
                'CUSTOM': 'Custom duration',
                'CUSTOM_QTD': 'Number of days'
            },
            'DESCRIPTION': {
                'DESC01': 'Unlimited LTE/4G internet',
                'DESC02': 'Unlimited calls in the US',
                'DESC03': '•	Unlimited calls to cellphones and landlines to over 75 countries',
                'DESC04': 'Unlimited SMS',
                'DESC05': 'Contact us to get the best plan option that fits your needs',
                'DESC06': 'Add extra days to your day'
            }
        },

    }
};
