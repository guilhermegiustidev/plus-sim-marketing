import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { HomeComponent } from './home.component';
import { SharedModule } from '../../../../core/modules/shared.module';

import { ServiceModule } from '../fragments/services/services.module';

import { PlanModule } from '../fragments/plans/plans.module';
import { CostumerModule } from '../fragments/costumer/costumer.module'; 
import { AboutUsModule } from '../fragments/about/about.us.module'; 
import { FactModule } from '../fragments/facts/facts.module';
import { WhereBuyModule } from '../fragments/wherebuy/where.buy.module';


const routes = [

    {
        path     : '',
        component: HomeComponent,
    }
];

@NgModule({
    declarations: [
        HomeComponent,
    ],
    imports     : [
        SharedModule,
        ServiceModule,
        PlanModule,
        CostumerModule,
        AboutUsModule,
        FactModule,
        WhereBuyModule,
        RouterModule.forChild(routes)
    ]
})

export class HomeModule
{

}
