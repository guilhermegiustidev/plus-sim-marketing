import { Component, OnInit } from '@angular/core'

import { locale as english } from './i18n/en';
import { locale as portuguese } from './i18n/pt';
import { locale as spanish } from './i18n/sp';

import { TranslateService } from '@ngx-translate/core';
import { TranslationLoader } from '../../../../core/services/translation-loader.service';
import { animations } from '../../../../core/animations';


declare var $: any;
declare var jquery: any;
declare var MasterSlider: any;


@Component({
	templateUrl: './home.component.html',
	styleUrls: ['./home.component.scss'],
	animations: animations

})
export class HomeComponent implements OnInit {

	constructor(
		private translationLoader: TranslationLoader,
		private translate: TranslateService,

	) {
		this.translationLoader.loadTranslations(english, portuguese, spanish);
		this.translate.use(localStorage.getItem('localeKey'));
	}

	ngOnInit() {
		"use strict";
		var slider = new MasterSlider();
		slider.control('arrows');
		slider.control('timebar', { insertTo: '#masterslider' });
		slider.control('bullets');

		slider.setup('masterslider', {
			width: 1400,    // slider standard width
			height: window.screen.width > 1000 ? window.screen.height : window.screen.height + 1000,   // slider standard height
			space: 1,
			layout: 'fullwidth',
			loop: true,
			preload: 0,
			instantStartLayers: true,
			autoplay: true
		});
		$("html, body").animate({ scrollTop: 0 });
	}


}