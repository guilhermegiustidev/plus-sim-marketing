export const locale = {
    lang: 'en',
    data: {
       'LABELS': {
            'LOGIN': 'Login',
            'REGISTER': 'Sign up',
            'LIVE.CHAT': 'Live chat',
            'CONTACT.US': 'Contact us',
            'TOOLBAR.LOCALE' : 'Language'
       }
    }
};
