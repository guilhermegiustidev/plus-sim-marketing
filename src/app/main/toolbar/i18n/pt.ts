export const locale = {
    lang: 'pt',
    data: {
       'LABELS': {
            'LOGIN': 'Login',
            'REGISTER': 'Cadastre-se',
            'LIVE.CHAT': 'Chat Online',
            'CONTACT.US': 'Fale conosco',
            'TOOLBAR.LOCALE' : 'Idioma'
       }
    }
};
