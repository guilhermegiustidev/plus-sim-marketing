export const locale = {
    lang: 'sp',
    data: {
       'LABELS': {
            'LOGIN': 'Login',
            'REGISTER': 'Cadastre-se',
            'LIVE.CHAT': 'Chat Online',
            'CONTACT.US': 'Fale conosco',
            'TOOLBAR.LOCALE' : 'Idioma'
       }
    }
};
