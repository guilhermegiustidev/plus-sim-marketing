import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { ToolbarComponent } from './toolbar.component';

import { SharedModule } from '../../core/modules/shared.module';

import { AuthenticationModule } from '../content/pages/authentication/authentication.module';
@NgModule({
  imports: [
    SharedModule,
    RouterModule,
    AuthenticationModule
  ],
  declarations: [
    ToolbarComponent
  ],
  exports: [
      ToolbarComponent
  ]
})
export class ToolbarModule { }
