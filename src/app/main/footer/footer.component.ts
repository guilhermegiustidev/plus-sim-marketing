import { Component } from '@angular/core'

import { locale as english } from './i18n/en';
import { locale as portuguese } from './i18n/pt';
import { locale as spanish } from './i18n/sp';

import { TranslateService } from '@ngx-translate/core';
import { TranslationLoader } from '../../core/services/translation-loader.service';

@Component({
    selector: 'footer-plus-sim',
    templateUrl: './footer.component.html',
    styleUrls: ['./footer.component.scss']

})
export class FooterComponent{

    constructor(
        private translationLoader: TranslationLoader,
        private translate: TranslateService
    ){
        this.translationLoader.loadTranslations(english, portuguese, spanish);
        this.translate.use(localStorage.getItem('localeKey'));

    }

}