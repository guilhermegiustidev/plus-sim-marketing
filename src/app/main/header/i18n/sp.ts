export const locale = {
    lang: 'sp',
    data: {
       'LABELS': {
            'LOGIN': 'Login',
            'REGISTER': 'Cadastre-se',
            'LIVE.CHAT': 'Chat Online',
            'CONTACT.US': 'Fale conosco',

            'HEADER': {
                'CART': {
                    'RECENT_ITEMS': 'Planos Adicionados'
                },
                'MENU': {
                    'WHO.WE.ARE' : 'Quem somos',
                    'FACTS' : 'Fatos sobre nós',
                    'SERVICES': 'Conheça nossos serviços',
                    'CONTACT_US': 'Fale conosco',
                    'SEE_PLANS': 'Veja nossos planos',
                    'BUILD_PLAN': 'Monte um plano customizado',
                    'BUY_ONLINE': 'Compre Online',
                    'BUY_CUSTOMERS' : 'Localize um representante', 
                    'BUY_WITH_US': 'Compre em nossos escritório',
                    'ACTIVE_TUTORIAL': 'Siga os passos para ativação',
                    'ACTIVE_LINE': 'Ative sua linha',
                    'COMMON_DOUBT': 'Dúvidas frequentes',
                    
                },
                'PLAN': 'Planos',
                'WHERE.BUY': 'Onde comprar',
                'ACTIVATE': 'Ative seu chip',
                'ABOUT.US': 'Sobre nós',
                'FAQ': 'FAQ',
                'WORK.WITH.US': 'Trabalhe conosco'
            }
       }
    }
};
