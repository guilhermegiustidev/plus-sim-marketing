﻿export const locale = {
    lang: 'en',
    data: {
       'LABELS': {
            'LOGIN': 'Login',
            'REGISTER': 'Sign up',
            'LIVE.CHAT': 'Live chat',
            'CONTACT.US': 'Contact us',

            'HEADER': {
                'CART': {
                    'RECENT_ITEMS': 'New plans'
                },
                'MENU': {
                    'WHO.WE.ARE' : 'Who we are',
                    'FACTS' : 'About us',
                    'SERVICES': 'Our services',
                    'CONTACT_US': 'Contact us',
                    'SEE_PLANS': 'Check our plans',
                    'BUILD_PLAN': 'Build a custom plan',
                    'BUY_ONLINE': 'Buy online',
                    'BUY_CUSTOMERS' : 'Find an agent', 
                    'BUY_WITH_US': 'Buy at our office',
                    'ACTIVE_TUTORIAL': 'Follow the steps to activate your PlusSim',
                    'ACTIVE_LINE': 'Active your line',
                    'COMMON_DOUBT': 'FAQ - Frequently Asked Questions',
                    
                },
                'PLAN': 'Plans',
                'WHERE.BUY': 'Where to buy',
                'ACTIVATE': 'Activate your PlusSim',
                'ABOUT.US': 'About us',
                'FAQ': 'FAQ',
                'WORK.WITH.US': 'Work with us'
            }
       }
    }
};
