import { Component, OnInit, Input } from '@angular/core'

import { locale as english } from './i18n/en';
import { locale as portuguese } from './i18n/pt';
import { locale as spanish } from './i18n/sp';

import { TranslateService } from '@ngx-translate/core';
import { TranslationLoader } from '../../core/services/translation-loader.service';
import { Router } from '@angular/router';


import { system_lang } from '../../core/settings/locale/languages.setting';

declare var $: any;
declare var jquery: any;


@Component({
    selector: 'header-plus-sim',
    templateUrl: './header.component.html',
    styleUrls: ['./header.component.scss']

})
export class HeaderComponent implements OnInit {

    selectedLang: any;

    @Input() sideNaveBar: any;

    langs = system_lang;


    @Input() parentMenus: Array<any>;

    constructor(
        private translationLoader: TranslationLoader,
        private translate: TranslateService,
        private router: Router
    ) {
        if (!localStorage.getItem('localeKey')) {
            localStorage.setItem('localeKey', this.langs[0].id);
        }
        this.setSelectedLocale();
        this.translationLoader.loadTranslations(english, portuguese, spanish);


    }

    private setSelectedLocale() {
        let locale = this.findLocaleById(localStorage.getItem('localeKey'));
        this.selectedLang = locale;
        this.translate.use(this.selectedLang.id);
    }

    private findLocaleById(id: string) {
        for (let locale of this.langs) {
            if (locale.id === id) {
                return locale;
            }
        }
        return null;
    }

    ngOnInit(): void {
        $('.yamm li.dropdown').on('click', function (event) {
            $('#navbar-collapse').removeClass('in');
       });
    }

    getItemsCart(){
        if(!localStorage.getItem('cart_size')){
            localStorage.setItem('cart_size', '0');
        }
        return localStorage.getItem('cart_size');
    }

    changeLang(event, lang: any) {
        event.preventDefault();
        localStorage.removeItem('localKey');
        localStorage.setItem('localeKey', lang.id);
        this.setSelectedLocale();
    }

    isMobile(){
        return window.screen.width < 1000;
    }

    scrollsTo(event, selectorId, pxs){
        event.preventDefault();
        if(this.router.url === '/'){
            $("html, body").animate({ scrollTop: $(selectorId).offset().top - pxs }, "slow");
        } else {
            this.backToIndex(event);
            setTimeout(() => {
                $("html, body").animate({ scrollTop: $(selectorId).offset().top - pxs }, "slow");
            }, 300)
        }
    }

    backToIndex(event){
        event.preventDefault();
        this.router.navigate(['']);
    }

    navigatoTo(event, url){
        event.preventDefault();
        this.router.navigate([url]);
    }

}