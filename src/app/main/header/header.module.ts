import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { HeaderComponent } from './header.component';

import { SharedModule } from '../../core/modules/shared.module';
import { FaqModule } from '../content/pages/faq/faq.module';
import { StoreModule } from '../content/pages/store/store.module';
import { CustomersModule } from '../content/pages/customers/customers.module';
import { OutLocationModule } from '../content/pages/out-location/out.location.module';
import { ContactModule } from '../content/pages/customer-contanct/customer.contanct.module';

import { ActivationModule } from '../content/pages/activation/activation.module';
import { CartModule } from '../content/pages/cart/cart.module';

@NgModule({
  imports: [
    SharedModule,
    FaqModule,
    StoreModule,
    ContactModule,
    CustomersModule,
    RouterModule,
    OutLocationModule,
    ActivationModule,
    CartModule
  ],
  declarations: [
    HeaderComponent
  ],
  exports: [
    HeaderComponent
  ]
})
export class HeaderModule { }
