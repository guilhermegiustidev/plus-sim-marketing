import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { TranslationLoader } from './core/services/translation-loader.service';
import { menu as menusApp } from './core/dto/menu';
import { system_lang } from './core/settings/locale/languages.setting';

declare var $: any;
declare var jquery: any;

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

    title = 'app';

    parentMenus: Array<any> = new Array();

    langs = system_lang;
    selectedLang: any;

    constructor(
        private translate: TranslateService,
        private translationLoader: TranslationLoader
    ) {
        // Add languages
        this.translate.addLangs(['en', 'pt', 'sp']);

        // Set the default language
        this.translate.setDefaultLang('pt');

        // Use a language
        this.translate.use('pt');

        // Set the navigation translations
        localStorage.setItem('localeKey', 'pt')

        for (let menu of menusApp) {
            if (!menu.parent) {
                this.parentMenus.push(menu);
            }
        }

        for (let parentMenu of this.parentMenus) {

            for (let menu of menusApp) {
                if (parentMenu.id === menu.parent) {
                    if (!parentMenu.children) {
                        parentMenu.children = new Array();
                    }
                    parentMenu.children.push(menu)
                }
            }

        }

    }

    ngOnInit(): void {

        $('#menuSideNav').cubeportfolio({
            filters: '#js-filters-faq',
            defaultFilter: 'plans',
            animationType: 'sequentially',
            gridAdjustment: 'default',
            displayType: 'default',
            caption: 'expand',
            gapHorizontal: 0,
            gapVertical: 0
        });
    }

    isSelected(lang: any){
        if(!this.selectedLang){
            this.setSelectedLocale();
        }
        return this.selectedLang.id === lang.id;
    }

    private setSelectedLocale() {
        let locale = this.findLocaleById(localStorage.getItem('localeKey'));
        this.selectedLang = locale;
        this.translate.use(this.selectedLang.id);
    }

    private findLocaleById(id: string) {
        for (let locale of this.langs) {
            if (locale.id === id) {
                return locale;
            }
        }
        return null;
    }


    changeLang(event, lang: any) {
        event.preventDefault();
        localStorage.removeItem('localKey');
        localStorage.setItem('localeKey', lang.id);
        this.setSelectedLocale();
    }

}
