
import { Routes } from '@angular/router';

export const appRoutes: Routes = [
   
    {
        path        : '',
        loadChildren: './main/content/pages/pages.module#PagesModule'
    },

    {path: '**', redirectTo: '/'}

];