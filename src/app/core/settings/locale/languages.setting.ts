export const system_lang = [
    {
        'id'   : 'pt',
        'title': 'Português',
        'flag' : 'br'
    },
    {
        'id'   : 'en',
        'title': 'English',
        'flag' : 'us'
    },
    {
        'id'   : 'sp',
        'title': 'Español',
        'flag' : 'sp'
    },

];