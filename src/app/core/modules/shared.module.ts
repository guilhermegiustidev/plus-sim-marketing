import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { TranslationLoader } from '../services/translation-loader.service';
import { TranslateModule } from '@ngx-translate/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { CommonModule } from '@angular/common';
import { MaterialModule } from './materials.module';
import { FlexLayoutModule } from '@angular/flex-layout';

@NgModule({

  imports: [
    MaterialModule,
    TranslateModule.forChild({}),
    CommonModule,
    FlexLayoutModule,
    FormsModule,
    ReactiveFormsModule,
  ],
  exports: [
    MaterialModule,
    CommonModule,
    TranslateModule,
    FlexLayoutModule,
    FormsModule,
    ReactiveFormsModule,    
  ],

  providers: [
    TranslationLoader
  ]
})
export class SharedModule { }
