import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { TranslationLoader } from '../services/translation-loader.service';

@NgModule({
  declarations: [
  ],
  exports: [
      
  ],
  providers: [
      TranslationLoader
  ]
})
export class ServiceModule { }
