export const plans = [
    {'id': '1','days': '5', 'price': 40, 'title' : 'LABELS.DAYS.FIVE', src: 'https://placeholdit.imgix.net/~text?txtsize=50&txt=600%C3%97400&w=600&h=400'},
    {'id': '3','days': '10', 'price': 50, 'title' : 'LABELS.DAYS.TEN', src: 'https://placeholdit.imgix.net/~text?txtsize=50&txt=600%C3%97400&w=600&h=400'},
    {'id': '4','days': '15', 'price': 60, 'title' : 'LABELS.DAYS.FIFTEEN', src: 'https://placeholdit.imgix.net/~text?txtsize=50&txt=600%C3%97400&w=600&h=400'},
    {'id': '5','days': '20', 'price': 70, 'title' : 'LABELS.DAYS.TWENTY', src: 'https://placeholdit.imgix.net/~text?txtsize=50&txt=600%C3%97400&w=600&h=400'},
    {'id': '6','days': '25', 'price': 80, 'title' : 'LABELS.DAYS.TWENTY_FIVE', src: 'https://placeholdit.imgix.net/~text?txtsize=50&txt=600%C3%97400&w=600&h=400'},
    {'id': '7','days': '30', 'price': 90, 'title' : 'LABELS.DAYS.THIRTY', src: 'https://placeholdit.imgix.net/~text?txtsize=50&txt=600%C3%97400&w=600&h=400'},
    {'id': '8','days': null, 'title' : 'LABELS.DAYS.MORE.THAN.THIRTY', src: 'https://placeholdit.imgix.net/~text?txtsize=50&txt=600%C3%97400&w=600&h=400'},
    {'id': '9','days': null, 'price': 2, 'title' : 'LABELS.DAYS.EXTRA', src: 'https://placeholdit.imgix.net/~text?txtsize=50&txt=600%C3%97400&w=600&h=400'}
];
