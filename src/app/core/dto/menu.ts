export const menu = [
    {'id': '1','name' :'plans', 'title' : 'LABELS.HEADER.PLAN', 'parent': '', 'link': '/comingSoon'},
    {'id': '2','name' :'wherebuy', 'title' : 'LABELS.HEADER.WHERE.BUY', 'parent': ''},
    {'id': '3','name' :'activate', 'title' : 'LABELS.HEADER.ACTIVATE', 'parent': '', 'link': '/activation'},
    {'id': '4','name' :'faq', 'title' : 'LABELS.HEADER.FAQ', 'parent': '', 'link': '/faq'},


    {'id': '7','name' :'buy_online', 'title' : 'LABELS.HEADER.MENU.BUY_ONLINE', 'parent': '2', 'link': '/store'},
    {'id': '8','name' :'customer_search', 'title' : 'LABELS.HEADER.MENU.BUY_CUSTOMERS', 'parent': '2', 'link': '/comingSoon'},
    {'id': '9','name' :'buy_in_office', 'title' : 'LABELS.HEADER.MENU.BUY_WITH_US', 'parent': '2', 'link': '/comingSoon'},




];
