import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import 'hammerjs';

import { SharedModule } from './core/modules/shared.module';

import { ToolbarModule } from './main/toolbar/toolbar.module';
import { HeaderModule } from './main/header/header.module';
import { FooterModule } from './main/footer/footer.module';

import { ServiceModule } from './core/modules/service.module';
import { AppComponent } from './app.component';
import { TranslateModule } from '@ngx-translate/core';
import { appRoutes } from './core/settings/page/routes.settring';


@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    HttpClientModule,
    BrowserAnimationsModule,
    BrowserModule,
    ToolbarModule,
    HeaderModule,
    ServiceModule,
    FooterModule,
    TranslateModule.forRoot(),
    RouterModule.forRoot(appRoutes),
    SharedModule

  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
